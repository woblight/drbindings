local addonName, addonTable = ...

_G[addonName] = addonTable

setmetatable(addonTable, {__index = getfenv() })
setfenv(1, addonTable)

local SaveBindings = SaveBindings or function() end
local LoadBindings = LoadBindings or function() end

local defaultSettings = {}
local defaultProfile = {}

local eventHandler = CreateFrame("frame")
eventHandler:RegisterEvent("ADDON_LOADED")
eventHandler:SetScript("OnEvent", function(self, event, ...) EFrame:atomic(self[event], ...) end)

keyFrame = CreateFrame("FRAME")
keyFrame:EnableKeyboard(true)
keyFrame:SetPropagateKeyboardInput(true)
keyFrame:SetScript("OnKeyDown", function (self, key)
    if IsKeyPressIgnoredForBinding(key) then return end
    local b = CreateKeyChordString(key)
    local bs = bindings()
    if not bs[b] and GetBindingAction(b, false) then
        DrBindingsSettings.extras[b] = true
    end
end)

function eventHandler.ADDON_LOADED(name)
    if name == addonName then
        if not DrBindingsProfile then
            _G.DrBindingsProfile = _G.DrBindingsProfile or { }
        end
        setmetatable(DrBindingsProfile, {__index = defaultProfile})
        if not DrBindingsSettings then
            _G.DrBindingsSettings = _G.DrBindingsSettings or { objects = {}, refs = { heads = {}, tags = {} }, extras = {} }
        end
        setmetatable(DrBindingsSettings, {__index = defaultSettings})
        if not DrBindingsProfile.HEAD then
            local specific = GetCurrentBindingSet() == CHARACTER_BINDINGS
            if not specific and getBranch("master") then
                DrBindingsProfile.HEAD = branchRef("master")
            else
                local branch = specific and UnitGUID("player") or "master"
                local message = specific and format("Initial commit for %s (%s)", (UnitName("player")), GetRealmName()) or "Initial commit"
                local bs = bindings()
                bss = bindingsSHA1(bs)
                getObjects()[bss] = bs
                local c = commit(message, bss)
                local cs = commitSHA1(c)
                getObjects()[cs] = c
                setBranch(branch, cs)
                DrBindingsProfile.HEAD = branchRef(branch)
            end
        end
    end
end

function printf(...)
    return print(format(...))
end

function head()
    return _G.DrBindingsProfile.HEAD
end

function setHead(head)
    _G.DrBindingsProfile.HEAD = head
end

function isHead(w)
    local head = head()
    return type(head) == "table" and head.ref == w or head == w
end

function branchRef(name)
    return { type = "ref", ref = "refs/heads/"..name }
end

function staged()
    return _G.DrBindingsProfile.staged
end
function setStaged(staged)
    _G.DrBindingsProfile.staged = staged
end

function clearStaged()
    setStaged(nil)
end

function setBranch(name, commit)
    _G.DrBindingsSettings.refs.heads[name] = commit
end

function getBranch(name)
    return _G.DrBindingsSettings.refs.heads[name]
end

function getBranches()
    return _G.DrBindingsSettings.refs.heads
end

function getObjects()
    return DrBindingsSettings.objects
end

local function _deref(a, b)
    return _G.DrBindingsSettings.refs[a][b]
end

function deref(ref)
    return type(ref) == "table" and ref.type == "ref" and deref(_deref(ref.ref:match("refs/(.*)/(.*)"))) or ref
end

function derefBranch(ref)
    return type(ref) == "table" and ref.type == "ref" and ref.ref:match("refs/heads/(.*)")
end

function derefCommit(v)
    local cc = deref(v)
    local c = getObject(cc)
    if not isCommit(c) then
        cc = search(v)
        c = getObject(cc)
    end
    if not isCommit(c) then
        return print("Invalid reference")
    end
    return cc, c
end

function getObject(sha1)
    return getObjects()[sha1]
end

function isCommit(w)
    return type(w) == "table" and w.type == "commit"
end

function conflictCheck(bs, diff)
    local conflicts = {}
    for k, v in pairs(diff) do
        if bs[k] ~= v.old then
            tinsert(conflicts, k)
        end
    end
    return #conflicts > 0 and conflicts or nil
end

function restore(bs)
    local r = true
    for k, v in pairs(bs) do
        if not SetBinding(k, v.action) then
            print(k, v.action)
            print("Failed to assign [%s] to [%s]", GetBindingName(v.action), k)
            r = false
            break
        end
    end
    if r then
        SaveBindings(GetCurrentBindingSet())
    else
        LoadBindings(GetCurrentBindingSet())
    end
    return r
end

function bindings(pattern)
    local btable = {}
    for i = 1, GetNumBindings() do
        local action, category = GetBinding(i)
        if not pattern or format("%s/%s",category,action):match(pattern) then
            local bindings = {select(3,GetBinding(i))}
            for _, v in ipairs(bindings) do
                assert(not btable[v], "Corrupted binding table")
                btable[v] = { action = action, category = category }
            end
        end
    end
    for k in pairs(DrBindingsSettings.extras) do
        if not btable[k] then
            local action = GetBindingAction(k, false)
            if action and #action > 0 then
                btable[k] = { action = action }
            end
        end
    end
    return btable
end

function diff2(b1, b2)
    local r = {}
    for k, v in pairs(b1) do
        local v2 = b2[k]
        if not v2 or v2.action ~= v.action then
            r[k] = v
        end
    end
    return r
end

TRACKED = 0
UNTRACKED = 1

function diff(b1, b2, mode)
    local o, n = (not mode or mode == TRAKED) and diff2(b1, b2) or {}, (not mode or mode == UNTRACKED) and diff2(b2, b1) or {}
    local d = {}
    for k, v in pairs(o) do
        d[k] = {old = v}
    end
    for k, v in pairs(n) do
        if not d[k] then
            d[k] = {new = v}
        else
            d[k].new = v
        end
    end
    return d
end

function commit(message, bref, ...)
    return { type = "commit",
             author_id = UnitGUID("player"),
             author_name = format("%s (%s)", (UnitName("player")), GetRealmName()),
             parents = {...},
             message = message,
             bref = bref,
             timestamp = time()
             }
end

function bindingsSHA1(b)
    local ord = {}
    for k, v in pairs(b) do
        tinsert(ord, {k,v})
    end
    table.sort(ord, function(a,b) return a[1] < b[1] end)
    local sha1 = sha2.sha1()
    for _, v in ipairs (ord) do
        sha1(v[1]) sha1(v[2].action)
    end
    return sha1()
end

function commitSHA1(c)
    local sha1 = sha2.sha1()
    sha1(c.author_id)
    sha1(c.author_name)
    for _, p in ipairs(c.parents) do
        sha1(p)
    end
    sha1(tostring(c.timestamp))
    sha1(c.message)
    sha1(c.bref)
    return sha1()
end
    

function actionToString(v, raw)
    if not raw then
        return format("%s/%s", v.category and _G[v.category] or "", GetBindingName(v.action))
    else
        return format("%s/%s", v.category or "", v.action)
    end
end

function showdiff(d)
    for k, v in pairs(d) do
        printf("%s: |c00ff0000%s|r -> |c0000ff00%s|r", GetBindingText(k, "KEY_", true), v.old and actionToString(v.old) or "unassigned", v.new and actionToString(v.new) or "unassigned")
    end
end

function shortsha(sha, n)
    n = n or 7
    local ssha = sha:sub(1,7)
    for k in pairs(getObjects()) do
        if k ~= sha and k:match("^"..ssha) then
            return shortsha(sha, n+1)
        end
    end
    return ssha
end

function printcommit(cc)
    local c = getObject(cc)
    local ishead = deref(head()) == cc
    local branches = {}
    for k, b in pairs(getBranches()) do
        if b == cc then
            tinsert(branches, k)
        end
    end
    if ishead and #branches > 0 then
        printf("|c00f67400commit %s|r (|c0016a085HEAD|r -> |c001cdc9a%s|r)", cc, table.concat(branches, ", "))
    elseif ishead then
        printf("|c00f67400commit %s|r (c0016a085HEAD|r)", cc)
    elseif #branches > 0 then
        printf("|c00f67400commit %s|r (|c001cdc9a%s|r)", cc, table.concat(branches, ", "))
    else
        printf("|c00f67400commit %s|r", cc)
    end
end

function search(sha)
    if type(sha) ~= "string" then return sha end
    local obj = getObject(sha)
    if obj then
        return sha
    end
    for k, v in pairs(getBranches()) do
        if sha == k then return v end
    end
    for k, o in pairs(getObjects()) do
        if k:match("^"..sha) then
            if not obj then
                obj = o
                sha = k
            else
                return "ambiguous reference"
            end
        end
    end
    return sha
end

local function chatcmd(msg)
    local args = {}
    local str
    for f in msg:gmatch("%S*") do
        if str or #f ~= 0 then
            local c = 0
            for fs in f:gmatch("\"") do
                c = c + 1
            end
            f = f:gsub("([^\\])\"","%1")
            f = f:gsub("^\"","")
            for fs in f:gmatch("\"") do
                c = c - 1
            end
            if str then
                if c % 2 == 0 then
                    str = str .. " " .. f
                else
                    tinsert(args, str .. " " .. f)
                    str = nil
                end
            else
                if c % 2 == 0 then
                    tinsert(args, f)
                else
                    str = f
                end
            end
        end
    end
    if str then
        return print("Missing closing \"")
    end
    local k,v = next(args)
    if v == "branch" then
        k,v = next(args, k)
        if not k then
            print("---")
            for k in pairs(DrBindingsSettings.refs.heads) do
                if head().ref == "refs/heads/"..k then
                    printf("|c0000ff00%s|r", k)
                else
                    print(k)
                end
            end
            print("---")
            return
        end
        local delete
        while v do
            if v == "-d" then
                delete = true
            elseif v:match("^%w+$") then
                local hasBranch = getBranch(v)
                if delete then
                    if hasBranch then
                        if isHead("refs/heads/"..v) then
                            return print("cannot delete current branch")
                        end
                        setBranch(v, nil)
                        return printf("branch %s deleted", v)
                    else
                        return printf("branch %s not found", v)
                    end
                else
                    if not hasBranch then
                        local commit = deref(head())
                        setBranch(v, commit)
                        setHead(branchRef(v))
                        return print("switched to new branch "..v)
                    else
                        return printf("branch %s alredy exists", v)
                    end
                end
            else
                break
            end
            k,v = next(args, k)
        end
    elseif v == "checkout" then
        k,v = next(args, k)
        if v == "HEAD" then
            local head = getObject(getObject(deref(head())).bref)
            restore(head)
        elseif not v then
            return help("checkout")
        else
            for bn, br in pairs(_G.DrBindingsSettings.refs.heads) do
                if bn == v then
                    if restore(getObject(getObject(br).bref)) then
                        setHead(branchRef(bn))
                        return printf("Now on branch %s", bn)
                    else
                        return print("Failed to restore bindings, reloaded last saved configuration.")
                    end
                end
            end
            for tn, tr in pairs(_G.DrBindingsSettings.refs.tags) do
                if tn == v then
                    if restore(getObject(getObject(tr).bref)) then
                        setHead({ type = "ref", ref = "res/tags/"..tn })
                        return printf("Now on tag %s", bn)
                    else
                        return print("Failed to restore bindings, reloaded last saved configuration.")
                    end
                end
            end
            if #v >= 5 then
                local result = search(v)
                local obj = getObject(result)
                if isCommit(obj) then
                    if restore(getObject(obj.bref)) then
                        setHead(result)
                        return printf("Now on commit %s", result)
                    else
                        return print("Failed to restore bindings, reloaded last saved configuration.")
                    end
                else
                    if not obj then
                        print("no valid reference provided")
                    else
                        print(result)
                    end
                    return
                end
            end
            return print("no valid reference provided")
        end
    elseif v == "diff" then
        k,v = next(args, k)
        local commit = getObject(deref(head()))
        showdiff(diff(getObject(commit.bref), bindings()))
    elseif v == "commit" then
        k,v = next(args, k)
        
        local message = v
        
        if not message then return print("Failed: empty commit message") end
        
        local bs = staged()
        if not bs then
            return print("Failed: nothing to commit")
        end
        bss = bindingsSHA1(bs)
        getObjects()[bss] = bs
        local cc = deref(head())
        local c = commit(message, bss, cc)
        local cs = commitSHA1(c)
        local branch = derefBranch(head())
        getObjects()[cs] = c
        if branch then
            setBranch(branch, cs)
        end
        clearStaged()
    elseif v == "add" then
        k,v = next(args, k)
        setStaged(bindings())
    elseif v == "rm" then
        k,v = next(args, k)
        clearStaged()
    elseif v == "status" then
        local head = head()
        if type(head) == "string" then
            printf("Detached HEAD %s", head)
        else
            local a, b = head.ref:match("^refs/(%w+)/(%w+)$")
            if a == "heads" then
                printf("On branch %s", b)
            elseif a == "tags" then
                printf("On tag %s", b)
            end
        end
        local hb = getObject(getObject(deref(head)).bref)
        if staged() then
            print("Staged changes:")
            showdiff(diff(hb, staged()))
        end
        print("Unstaged changes:")
        showdiff(diff(staged() or hb, bindings(), TRACKED))
        print("Untracked changes:")
        showdiff(diff(staged() or hb, bindings(), UNTRACKED))
        
    elseif v == "cherry-pick" then
        k,v = next(args, k)
        local bs = bindings()
        local hc, hcc = derefCommit(head())
        local hbs = getObject(hcc.bref)
        if next(diff(hbs, bs, TRACKED)) then
            return print("Error: uncommitted changes present.")
        end
        local cc, c = derefCommit(v)
        local cbs = getObject(c.bref)
        if next(diff(cbs, bs, TRACKED)) then
            return print("Error: untracked changes would get overrided.")
        end
        local d = diff(getObject(getObject(c.parents[1]).bref), cbs)
        local err
        for key, dd in pairs(d) do
            local a = hbs[key]
            if not a or a.action  ~= dd.old.action then
                printf("Conflicting key [%s], HEAD: %s, %s: %s", key, a and a.action or "nothing", shortsha(c.parents[1]), dd.old.action or "nothing")
                err = true
            end
        end
        if err then return end
        local nbs = {}
        for k, b in pairs(hbs) do
            nbs[k] = b
        end
        for key, dd in pairs(d) do
            nbs[key] = dd.new
            SetBinding(key, dd.new and dd.new.action or nil)
        end
        nbss = bindingsSHA1(nbs)
        c = commit(format("Cherry-pick %s\n%s", cc, c.message), nbss, hc)
        cc = commitSHA1(c)
        getObjects()[nbss] = nbs
        getObjects()[cc] = c
        local branch = derefBranch(head())
        if branch then
            setBranch(branch, cc)
        end
        SaveBindings(GetCurrentBindingSet())
    elseif v == "show" then
        k,v = next(args, k)
        if not k or v == "HEAD" then
            v = head()
        end
        local cc, c = derefCommit(v)
        if not c then
            return print("Invalid reference")
        end
        printcommit(cc)
        printf("Author: %s <%s>", c.author_name, c.author_id)
        printf("Date: %s", date("%a %b %d %X",c.timestamp))
        printf(c.message)
        printf(" ")
        showdiff(diff(c.parents[1] and getObject(getObject(c.parents[1]).bref) or {}, getObject(c.bref)))
    elseif v == "log" then
        k,v = next(args, k)
        if not k or v == "HEAD" then
            v = head()
        end
        local cc = derefCommit(v)
        while cc do
            local c = getObject(cc)
            printcommit(cc)
            printf("Author: %s <%s>", c.author_name, c.author_id)
            printf("Date: %s", date("%a %b %d %X",c.timestamp))
            printf(c.message)
            printf(" ")
            cc = c.parents[1]
        end
    else
        k,v = next(args, k)
        help(v)
    end
end

function help(w)
    if w == "add" then
        print("/drb add")
        print("    prepares current bindings for commit")
    elseif w == "branch" then
        print("/drb branch [-d] [<branch_name>]")
        print("    with no arguments it lists branches, if branch_name is provided it creates a branch with that name")
        print("    -d branch_name, deletes a branch")
    elseif w == "checkout" then
        print("/drb checkout <branch>")
        print("/drb checkout <tag>")
        print("/drb checkout <commit>")
        print("    applies bindings from named branch, tag or commit")
    elseif w == "commit" then
        print("/drb commit <message>")
        print("    creates a commit with currently prapared changes, if <message> contains spaces it must be enclosed between \"message with spaces\"")
    elseif w == "cherry-pick" then
        print("/drb cherry-pick <commit>")
        print("    applies the changes introduced by the commit as a new commit on the top of the current one. Requires a clean working status.")
    elseif w == "diff" then
        print("/drb diff")
        print("    shows differences between current bindings, prepared changes and current commit")
    elseif w == "log" then
        print("/drb log [<branch>]")
        print("/drb log [<tag>]")
        print("/drb log [<commit>]")
        print("    shows the commit history for the branch, tag, commit or current commit if nothing specified")
    elseif w == "rm" then
        print("/drb rm")
        print("    reset prepared changes")
    elseif w == "show" then
        print("/drb show [<branch>]")
        print("/drb show [<tag>]")
        print("/drb show [<commit>]")
        print("    prints information and changes of the commit, current one if none specified")
    elseif w == "status" then
        print("/drb status")
        print("    prints informations about current state of things")
    else
        print("DrBindings commands")
        print("  /drb add - stages modifications for commit")
        print("  /drb branch - list branches, create a new branch, delete a branch")
        print("  /drb checkout - load bindings from a branch, tag or commit")
        print("  /drb commit - create a commit with currently staged changes")
        print("  /drb cherry-pick - applies the changes introduced by a commit as a new commit on the top of the current one")
        print("  /drb diff - prints the bindings changing between two commits")
        print("  /drb log - list a commit and its parent commits")
        print("  /drb rm - unstages modifications")
        print("  /drb show - prints a commit and affected bindings")
        print("  /drb status - prints current status")
        print("  /drb help command - shows help for command")
    end
end

_G.SLASH_DRBINDINGS1 = "/drb"
_G.SlashCmdList["DRBINDINGS"] = chatcmd

